import set from 'lodash.set';
import get from 'lodash.get';
import forOwn from 'lodash.forown';
import isempty from 'lodash.isempty';
import clone from 'lodash.clonedeep';
import cloneWith from 'lodash.clonedeepwith';

import { Config, FireDoc, PropertyPathSegment, KeyedCollection, AnyCollection, FSObj } from './vue-computed-firestore-types';

// const get = require('lodash.get');
// const forOwn = require('lodash.forown');
// const isempty = require('lodash.isempty');
// const clone = require('lodash.clonedeep');

// Define base config option
// This is overwritten by an options passed to Vue.use
// Then, can be overwritten at a per-component level and a per-property level
let baseConfig: Config = {
  firebase: undefined,
  baseName: '$fs',
  definitionName: 'computedFirestore',
  includeKey: '$key',
  includeMetadata: '$metadata',
  collectionsAsObject: true,
  followReferences: true,
  handlers: {
    error: [],
    once: [],
    populated: [],
    updated: [],
    added: [],
    removed: [],
    modified: [],
    changed: [],
  },
};

function mergeConfigs(oldConfig: Config, newConfig: Config, mode: string = ''): Config {
  const out: Config = clone(oldConfig);
  forOwn(newConfig, (val: any, key: keyof Config) => {
    if (key === 'baseName') {
      if (mode === 'install') {
        out.baseName = val as string;
      }
    } else if (key === 'handlers') {
      forOwn(newConfig.handlers, (handlers: Function[] | Function, handlerKey: keyof Config["handlers"]) => {
        if (Array.isArray(handlers)) {
          out.handlers[handlerKey].push(...handlers);
        } else {
          out.handlers[handlerKey] = [handlers];
        }
      });
    } else {
      out[key] = val as any;
    }
  });
  return out;
}

function callAllInArray(arr: Function[], ...args: any) {
  arr.forEach(f => f(...args));
}

// used to create added/modified/removed/updated 'collections'
function pushOrSet(targ: any, obj: any, id?: string | number) {
  if (Array.isArray(targ)) {
    targ.push(obj);
  } else {
    targ[id] = obj as any;
  }
}


const beforeCreate = function beforeCreate() {
  if (this.$options[baseConfig.definitionName]) {
    const bindings = this.$options[baseConfig.definitionName].call(this);
    const conf: Config = bindings.$config || {};
    const localConfig: Config = mergeConfigs(baseConfig, conf);

    // because we don't want to treat it as a firebase binding
    delete bindings.$config;
    const baseObj: FSObj = { $values: {}, $watchers: {} };

    Object.defineProperty(baseObj, '$config', {
      enumerable: false,
      value: localConfig,
    });
    Object.defineProperty(baseObj, '$watchers', {
      enumerable: false,
      value: {},
    });
    this.$set(baseObj, '$values', {});
    // pass baseObj through to data function
    this.$options[baseConfig.baseName] = baseObj;
    // array of promises which, when completed, call populated callbacks
    const populatedArr: Promise<never>[] = [];
    // iterate through all the bindings the user has given us
    forOwn(bindings, (binding, propName) => {
      // ensure we have a valid object if the binding returns no results
      this.$set(baseObj.$values, propName, {});
      this.$options.computed[propName] = {
        get() {
          return this.$data[baseConfig.baseName].$values[propName];
        },
        set(val: object) {
          // throw if they try to directly update a collection
          if (!this.$data[baseConfig.baseName].$values[propName].$isDoc) {
            throw new Error('vue-computed-firestore: Can\'t directly set collection');
          }
          this.$data[baseConfig.baseName].$values[propName].$set(val);
        },
      };

      // keep promise resolver externally, allowing us to call it when the first snapshot arrives
      let populatedResolve: Function;
      populatedArr.push(new Promise((resolve) => {
        populatedResolve = resolve;
      }));

      let firstRun = true;
      // if the first element of the binding is an object, treat it like a config
      const bindingConf = typeof binding[0] === 'object' ? binding[0] : {};
      const bindingConfig = mergeConfigs(localConfig, bindingConf);
      if (typeof binding[0] === 'object') {
        // then remove it, becuase it's not actually part of the path
        binding.shift();
      }

      // handle creation of property, allowing us to set it directly
      const createProp = (outDoc: FireDoc, prop: string, val: any, ref: any, followReferences: number | boolean | string, path: string[], root: FireDoc) => {
        // it's a reference type
        let outVal: any = val;

        let isRef = false;
        if (typeof val === 'object') {
          if (val.constructor.name === 'DocumentReference') {
            // reference type
            isRef = true;
            if (followReferences) {
              // track changes to the subdocument
              const newRef: number | boolean | string = typeof followReferences === 'number' ? followReferences - 1 : followReferences;
              val.onSnapshot((data: any) => {
                this.$set(outDoc, prop, createDocument(data, newRef))
              });
              // set to blank object initially
              outVal = {};
            } else {
              // set to blank string
              outVal = val.path;
            }
          } else {
            // map/object or array
            Object.keys(val).forEach((k) => {
              createProp(outVal, k, val[k], ref, followReferences, [...path, prop], root)
            });
          }
          outVal = new Proxy(outVal, {
            set(targ, key: any, v) {
              // allow Vue to extend prototypes
              if (key === '__proto__') return true;
              if (key in targ) {
                console.log('normal setter');

                // route through normal setters if available
                targ[key] = v;
                return true;
              }
              console.log('new-val setter', path);
              const rootPath = path[0] || prop;
              // otherwise, directly call update
              const newObj = {
                [rootPath]: cloneWith(root[rootPath], (v) => {
                  console.log('clone inner', v);

                  // either v.$ref (for doc references) or let clone cover it itself
                  // in the case that val.$ref is undefined
                  return v.$ref;
                }),
              };
              console.log('newObj cloned', newObj);

              set(newObj, [...path, prop, key].join('.'), v)
              console.log(newObj);

              ref.update(newObj)
              return true;
            }
          })
        }

        Object.defineProperty(outDoc, prop, {
          get: () => outVal,
          set: v => {
            // if we're setting it to a doc (i.e., it's a reference type)
            // don't trigger update because we're in fact filling out data from a 
            // sub-reference that we've retrieved
            if (v.$isDoc) {
              outVal = v;
            } else {
              if (isRef && v.constructor.name !== 'DocumentReference' && typeof v === 'object') {
                // if we're updating with a reference-prop directly (with this.x.reference = {}), 
                // actually update the document it's referring to
                // note this is only for objects - if we're trying to set it with a primitive value
                // then save that within the parent
                val.set(v)
              } else {
                // otherwise update the prop itself - for all data types
                // deal with cloning objects/maps/arrays, as well as primitives
                const rootPath = path[0] || prop;
                const newObj = {
                  [rootPath]: cloneWith(root[rootPath], (val) => {
                    // either val.$ref (for doc references) or let clone cover it itself
                    // in the case that val.$ref is undefined
                    return val.$ref;
                  }),
                };
                set(newObj, [...path, prop].join('.'), v)
                ref.update(newObj)
              }
            }
          },
          enumerable: true,
          configurable: true,
        });
      };

      // handle creation of a document, appending non-enumerable properties
      const createDocument = (docSnap: any, followReferences: number | boolean | string): FireDoc => {

        const out: FireDoc = {};
        forOwn(docSnap.data(), (val, prop) => {
          createProp(out, prop, val, docSnap.ref, followReferences, [], out);
        });
        if (bindingConfig.includeKey) {
          Object.defineProperty(out, bindingConfig.includeKey, {
            enumerable: false,
            value: docSnap.id,
          });
        }
        if (bindingConfig.includeMetadata) {
          Object.defineProperty(out, bindingConfig.includeMetadata, {
            enumerable: false,
            value: docSnap.metadata,
          });
        }
        Object.defineProperty(out, '$update', {
          enumerable: false,
          value: (d: Object) => docSnap.ref.update(d),
        });
        Object.defineProperty(out, '$isDoc', {
          enumerable: false,
          value: true,
          configurable: false,
          writable: false,
        });
        Object.defineProperty(out, '$set', {
          enumerable: false,
          value: (d: Object) => docSnap.ref.set(d),
        });
        Object.defineProperty(out, '$delete', {
          enumerable: false,
          value: () => docSnap.ref.delete(),
        });
        Object.defineProperty(out, '$ref', {
          enumerable: false,
          value: docSnap.ref,
        });
        // Create a Proxy to catch writes to non-extant properties, and to re-route sets to update
        return new Proxy(out, {
          set(targ, key: any, val) {
            // allow Vue to extend prototypes
            if (key === '__proto__') return true;
            if (key in targ) {
              // route through createProp setters if available
              targ[key] = val;
              return true;
            }
            docSnap.ref.update({ [key]: val });
            return true;
          },
        });
      };

      // create a collection-like Object or Array
      const createCollection = (querySnap: any, ref: any): AnyCollection => {
        let out: AnyCollection;
        // it's a query or collection
        if (bindingConfig.collectionsAsObject) {
          out = querySnap.docs.reduce((acc: {}, doc: any) => (
            {
              [doc.id]: createDocument(doc, bindingConfig.followReferences),
              ...acc,
            }
          ), {});
        } else {
          out = querySnap.docs.map((doc: Object) => createDocument(doc, bindingConfig.followReferences));
        }
        if (ref && ref.add) {
          Object.defineProperty(out, '$add', {
            enumerable: false,
            value: (d: Object) => ref.add(d),
          });
        }

        Object.defineProperty(out, '$ref', {
          enumerable: false,
          value: querySnap,
        });

        Object.defineProperty(out, '$isDoc', {
          enumerable: false,
          value: false,
          configurable: false,
          writable: false,
        });
        // Create a Proxy to catch writes to non-extant properties, and to re-route sets to $set
        return new Proxy(out, {
          set(targ, key: any, val) {
            // allow Vue to extend prototypes
            if (key === '__proto__') return true;
            // key already exists (array or object)
            if (key in targ) {
              (targ as any)[key].$set(val);
            } else {
              // target is object, we have the new key
              if (bindingConfig.collectionsAsObject) {
                ref.doc(key).set(val);
              } else {
                // target is array, set a new key
                ref.add(val);
              }
            }
            return true;
          },
        });
      };


      const unwatchers: Function[] = [];

      // define scoped watch adder
      const getAndWatch = (val: string) => {
        let out = val;
        if (out[0] === '$') {
          unwatchers.push(
            // eslint-disable-next-line no-use-before-define
            this.$watch(out.slice(1), () => computed()),
          );
          out = get(this, out.slice(1));
        }
        return out;
      };

      // blank unsub function so we can just use unsub() instead of checking typeof===function
      let unsub: Function = (): void => undefined;

      let outVal: FireDoc | FireDoc[] | KeyedCollection = {};

      /**
       * Computed
       *
       * Function that runs (per defined binding) whenever one of the reactive properties selecting
       * a ref changes, updating the ending ref and consequently updating the reactive object
       *
       * @param {Boolean} kill - True to detach listeners. Used in beforeDestroy
       */
      const computed = (kill = false) => {
        // kill previous snapshot listener and reactive watchers
        unsub();
        unwatchers.forEach(un => un());
        unwatchers.length = 0;
        if (kill) return;
        let internalFirstRun = true;

        let ref = bindingConfig.firebase.firestore();

        // refType (of the tail of the ref stack)
        // false for document (or to start with), true for collection
        let refType = false;

        // iterate each part of the binding
        binding.forEach((nextPoint: PropertyPathSegment) => {
          // strings are really simple - it's a collection or doc ID
          if (typeof nextPoint === 'string') {
            const nextID = getAndWatch(nextPoint);
            if (refType) {
              ref = ref.doc(nextID);
            } else {
              ref = ref.collection(nextID);
            }
          } else if (Array.isArray(nextPoint)) {
            // it's a query
            const nextQuery = nextPoint.slice(0, 3)
              .map(queryComponent => getAndWatch(queryComponent));
            ref = ref.where(...nextQuery);
          } else {
            // its a limit/order
            if (nextPoint.orderBy) {
              if (Array.isArray(nextPoint.orderBy)) {
                const orderByParams = nextPoint.orderBy.slice(0, 2)
                  .map(orderByParam => getAndWatch(orderByParam));
                ref = ref.orderBy(...orderByParams);
              } else {
                ref = ref.orderBy(getAndWatch(nextPoint.orderBy));
              }
            }
            if (nextPoint.limit) {
              ref = ref.limit(getAndWatch(nextPoint.limit));
            }
          }

          refType = !refType;
        });
        let isDoc = false;
        // keep a reference to the unsubscribe function
        unsub = ref.onSnapshot((data: any) => {
          if (data.docs) {
            // it's a collection-like
            outVal = createCollection(data, ref);
          } else {
            // it's a document
            outVal = createDocument(data, bindingConfig.followReferences);
          }
          const added = bindingConfig.collectionsAsObject ? {} : [];
          const modified = bindingConfig.collectionsAsObject ? {} : [];
          const updated = bindingConfig.collectionsAsObject ? {} : [];
          // removed is special - it's always an array of IDs
          const removed: Object[] = [];
          if (data.docChanges) {
            // deal with changes
            data.docChanges().forEach((change: any) => {
              const modID: string | number = bindingConfig.collectionsAsObject ? change.doc.id : change.newIndex;
              switch (change.type) {
                case 'added':
                  pushOrSet(added, (outVal as any)[modID], modID);
                  pushOrSet(updated, (outVal as any)[modID], modID);
                  break;
                case 'modified':
                  pushOrSet(modified, (outVal as any)[modID], modID);
                  pushOrSet(updated, (outVal as any)[modID], modID);
                  break;
                case 'removed':
                  pushOrSet(removed, change.doc.id, modID);
                  pushOrSet(updated, (outVal as any)[modID], modID);
                  break;
                default:
              }
            });
          } else {
            // we want to call modified and updated after setting this[propName]
            isDoc = true;
          }
          // set the reactive property - this humble line is probably the most important
          this.$set(this.$data[baseConfig.baseName].$values, propName, outVal);
          if (isDoc) {
            // call modified and updated for a single document
            callAllInArray(bindingConfig.handlers.modified, outVal);
            callAllInArray(bindingConfig.handlers.updated, outVal);
          }
          // if any of these have contents, trigger their callbacks
          if (!isempty(modified)) {
            callAllInArray(bindingConfig.handlers.modified, modified);
          }
          if (!isempty(removed)) {
            callAllInArray(bindingConfig.handlers.removed, removed);
          }
          if (!isempty(added)) {
            callAllInArray(bindingConfig.handlers.added, added);
          }
          if (!isempty(updated)) {
            callAllInArray(bindingConfig.handlers.updated, updated);
          }
          if (firstRun) {
            firstRun = false;
            // call once handlers, and resolve external promise
            callAllInArray(bindingConfig.handlers.once, outVal);
            populatedResolve();
          }
          if (internalFirstRun) {
            internalFirstRun = false;
            callAllInArray(bindingConfig.handlers.changed, outVal);
          }
        }, (error: any) => {
          // call error handlers
          callAllInArray(bindingConfig.handlers.error, error);
        });
      };
      // end computed

      // store a reference to the computed function - this allows us to actually trigger it on
      // created, as well as clean up on beforeDestroy
      baseObj.$watchers[propName] = computed;
    });
    // once all populated promises resolve, call populated handlers
    Promise.all(populatedArr).then(() => {
      callAllInArray(localConfig.handlers.populated);
    });
  }
};

const data = function data() {
  if (!this.$options[baseConfig.baseName]) return {};
  // append our watchers, values etc to this.$data
  return {
    [baseConfig.baseName]: this.$options[baseConfig.baseName],
  };
};

const created = function created() {
  if (!this.$data[baseConfig.baseName]) return;
  // start computed callbacks
  forOwn(this.$data[baseConfig.baseName].$watchers, cb => cb());
};

const beforeDestroy = function beforeDestroy() {
  if (!this.$data[baseConfig.baseName]) return;
  // tidy up computed callbacks
  forOwn(this.$data[baseConfig.baseName].$watchers, cb => cb(true));
};
const mixin = {
  data,
  beforeCreate,
  created,
  beforeDestroy,
};

export default function install(Vue: any, options: any) {
  if (!options.firebase) {
    throw Error('No Firebase instance provided to vue-computed-firestore');
  }
  // set up base config - this can be overwritten per binding
  baseConfig = mergeConfigs(baseConfig, options, 'install');
  Vue.mixin(mixin);
}
