export interface Config {
  firebase?: undefined | {firestore: Function};
  baseName?: string;
  definitionName?: string;
  includeKey?: string;
  includeMetadata?: string;
  // Need to have |string here for some reason
  collectionsAsObject?: boolean | string;
  followReferences?: boolean | number | string;
  handlers?: {
    error?: Function[];
    once?: Function[];
    populated?: Function[];
    updated?: Function[];
    added?: Function[];
    removed?: Function[];
    modified?: Function[];
    changed?: Function[];
  };
}
export interface FireDoc {
  $key?: string;
  $update?: Function;
  $set?: Function;
  $delete?: Function;
  $metadata?: Object;
  $isDoc?: true;
  [prop: string]: any;
}

export interface KeyedCollection {
  $add?: Function;
  $isDoc?: false;
  [prop: string]: any;
}

export interface ArrayCollection {
  $add?: Function;
  $isDoc ?: false;
  [prop: number]: any;
}

export type PropertyPathSegment = string | string[] | {orderBy?: string|string[], limit: string};

export type AnyCollection = KeyedCollection | ArrayCollection;

export type DocOrCol = AnyCollection | FireDoc;

export interface FSObj {
  $watchers: {
    [prop: string]: Function,
  },
  $values: {
    [prop: string]: DocOrCol,
  }
}